# -*- coding: utf-8 -*-
{
    'name': "GfK Survey",
    'summary': """
        Mart Surveying Information System""",
    'description': """
        Mart Surveying Information System for GfK
    """,
    'author': "Arkana",
    'website': "https://www.arkana.co.id",
    'category': 'Tools',
    'version': '0.1',
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
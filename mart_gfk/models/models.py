# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class gfk(models.Model):
#     _name = 'gfk.gfk'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class Building(models.Model):
	_name = "building.building"

	name = fields.Char("Name")
	street = fields.Char("Street")
	active = fields.Boolean("Active")

#questionnaire

class UCQ(models.Model):
	_inherit = "res.partner"
	
	questioner_code = fields.Char("Questioner Code")
	# UC Area
	state_id = fields.Many2one("res.country.state", "Propinsi")
	dati = fields.Selection([('city','Kota'),('district', 'Kabupaten')], "Dati")
	city_id = fields.Many2one('res.state.city', 'Kota/Kabupaten')
	kecamatan_id = fields.Many2one('res.city.kecamatan','Kecamatan')
	kelurahan_id = fields.Many2one('res.kecamatan.kelurahan','Kecamatan')
	zip = fields.Char("Kode Pos")

	# Shop Information
	name = fields.Char(size=32)
	building_name_id = fields.Many2one('bulding.building',"Building Name")
	floor = fields.Char("Floor")
	street = fields.Char("Street", size=64)
	block = fields.Char("Blok")
	block_no = fields.Char("No")
	phone = fields.Char("Phone No.")
	store_loc_type = fields.Selection([("street", "Street"),("mall", "Mall"),("market", "Market"),("shop", "Shop"),("sis", "S.I.S")], "Store Location")
	store_length = fields.Integer("Panjang")
	store_width = fields.Integer("Lebar")
	store_size = fields.Selection([("xl", "XL"), ("l", "L"), ("m", "M"), ("s", "S"), ("xs", "XS")], "Store Size")
	rent_fee = fields.Float("Rent Fee", help="Rent Fee per Year")
	visiting_evidence = fields.Selection([("name_card", "Name Card"),("stamp", "Stamp"),("receipt", "Receipt"),("brochure", "Brochure"),("photo", "Photo")], "Visiting Evidence")
	age_of_store = fields.Float("Age of Store")
	respondent_name = fields.Char("Respondent Name", size=32)
	shop_status = fields.Selection([("panel","Panel"),("non_panel","Non-Panel"),], "Shop Status")
	# check_out_counter = 

	# persentase display
	audio_visual_prcntg = fields.Float("Audio Visual Percentage")
	home_entertainment_prcntg = fields.Float("Home Entertainment Percentage")
	sda_prcntg = fields.Float("SDA Percentage")
	mda_prcntg = fields.Float("MDA Percentage")
	photo_prcntg = fields.Float("Photo Percentage")
	it_prcntg = fields.Float("IT Percentage")
	telco_prcntg = fields.Float("Telecom Percentage")
	audio_prcntg = fields.Float("Audio Percentage")
	lightning_prcntg = fields.Float("Lightning Percentage")
	other_prcntg = fields.Float("Other Percentage")
	total_main_prcntg = fields.Float("Total Percentage")

	it_dpc_prcntg = fields.Float("IT (DPC/PPC) Percentage")
	it_peripheral_prcntg = fields.Float("IT Peripherals Percentage")
	it_accessories_prcntg = fields.Float("IT Accessories Percentage")
	it_component_prcntg = fields.Float("IT Networking Percentage")
	it_networking_prcntg = fields.Float("IT Networking Percentage")
	it_other_prcntg = fields.Float("IT Other Percentage")
	total_it_prcntg = fields.Float("Total Percentage")

	# Shop Type
	electrical_type = fields.Selection(
			[
				('avs','AVS (Audio Visual Store)'),
				('ges','GES (General Electro Store)'),
				('wgs','AVS (Audio Visual)'),
				('cbs','AVS (Audio Visual)'),
				('sda','AVS (Audio Visual)'),
				('air','AVS (Audio Visual)'),
				('im','AVS (Audio Visual)'),
				('glw','AVS (Audio Visual)'),
				('ls','AVS (Audio Visual)'),
			], "Electrical"
		)
	audio_type = fields.Selection(
			[
				('cgr','AVS (Audio Visual)'),
				('cts','AVS (Audio Visual)'),
				('ego','AVS (Audio Visual)'),
			],"Audio"
		)
	telco_type = fields.Selection(
			[
				('tcs','AVS (Audio Visual)'),
				('tcs2','AVS (Audio Visual)'),
				('tca','AVS (Audio Visual)'),
				('vsp','AVS (Audio Visual)'),
				('mbs','AVS (Audio Visual)'),
			],"Telecom"
		)
	computer_type = fields.Selection(
			[
				('its','AVS (Audio Visual)'),
				('iass','AVS (Audio Visual)'),
				('ibrd','AVS (Audio Visual)'),
				('inbs','AVS (Audio Visual)'),
				('ipps','AVS (Audio Visual)'),
				('prt','AVS (Audio Visual)'),
				('wbk','AVS (Audio Visual)'),
				('gdt','AVS (Audio Visual)')
			], "Computer"
		)
	photo_type = fields.Selection(
			[
				('pho','AVS (Audio Visual)'),
				('ml','AVS (Audio Visual)'),
			], "Photo"
		)

	# Product Brand and Display
	# Audio Visual
	av_brand = [
			('lg','LG'),
			('sharp', 'Sharp'),
			('panasonic', 'Panasonic'),
			('polytron', 'Polytron'),
			('samsung', 'Samsung'),
			('sony', 'Sony'),
			('gmc', 'GMC'),
			('others', 'Others')
		]

	av_uhd_tv = fields.Selection(av_brand, "UHD TV")
	av_p_tv = fields.Selection(av_brand, "PTV (TV LCD/LED/PLASMA)")
	av_ahs = fields.Selection(av_brand, "AHS (Mini Midi/Home Theater)")
	av_uhd_tv_qty = fields.Float("Total Unit")
	av_p_tv_qty = fields.Float("Total Unit)")
	av_ahs_qty = fields.Float("Total Unit)")
	av_headphone_qty = fields.Float("Headphone")
	av_radio_recorder_qty = fields.Float("Radio/Recorder (Kaset) Player")
	av_player_qty = fields.Float("Player")
	av_clock_radio_qty = fields.Float("Clock Radio")
	av_speaker_qty = fields.Float("Speaker (Besar/Salon)")
	av_sound_bar_qty = fields.Float("Sound Bar/Soundplate/Lap341")
	av_crt_tv_qty = fields.Float("CRT-TV (TV Tabung)")
	av_camcorder_qty = fields.Float("Camcorder (Handycam)")
	av_dvd_player_qty = fields.Float("DVD/Blue-Ray Player")
	av_projector_qty = fields.Float("Projector")
	av_others = None

	# Lightning
	lit_brand = [
		('omi','OMI'),
		('philips','Philips'),
		('hannochs','Hannochs'),
		('panasonic','Panasonic'),
		('osram','osram'),
		('atama','Atama'),
		('others','Others'),
	]
	lit_cfl = fields.Selection(lit_brand, "Compact Flurescent Lamp (CFL) (Lampu Hemat Energi)")
	lit_luminnaire = fields.Selection(lit_brand, "Luminnaire (Downlight & Batten)")
	lit_tl = fields.Selection(lit_brand, "Fluorescent Lamp (Lampu TL)")
	lit_led = fields.Selection(lit_brand, "LED Lamp")
	lit_halogen_qty = fields.Float("Halogen")
	lit_incandescent_qty = fields.Float("Incandescent Lamp (Lampu Pijar/Bohlam)")
	lit_material_qty = fields.Float("Electrical Material (Kabel, Stop Kontak, Saklar)")
	lit_others = None

	# MDA dan SDA
	msda_brands = [
		('lg','LG'),
		('sharp', 'Sharp'),
		('panasonic', 'Panasonic'),
		('polytron', 'Polytron'),
		('samsung', 'Samsung'),
		('aqua', 'Aqua'),
		('rinnai', 'Rinnai'),
		('others', 'Others'),
	]

	msda_washing_machine_double_tube = fields.Selection(msda_brands, "Washing Machine Double Tube (Mesin Cuci Dua Tabung)")
	msda_washing_machine_single_tube = fields.Selection(msda_brands, "Washing Machine Single Tube (Mesin Cuci Satu Tabung)")
	msda_washing_machine_front = fields.Selection(msda_brands, "Washing Machine (Mesin Cuci) Front Loading")
	msda_ac = fields.Selection(msda_brands, )
	msda_ = fields.Selection(msda_brands, )
	msda_ = fields.Selection(msda_brands, )
	msda_ = fields.Selection(msda_brands, )
	msda_ = fields.Selection(msda_brands, )
	msda_ = fields.Float()
	msda_ = fields.Float()
	msda_ = fields.Float()

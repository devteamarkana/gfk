{
'name':'Kota',
'description':'Digunakan untuk menambah pilihan kota pada Odoo',
'author':'Erlangga Indra Permana',
'website':'https://erlangga.github.io',
'depends':['base'],
'data':[
        'city_view.xml',
        'state_country_data.xml',
        'ir.model.access.csv',
        ],
}